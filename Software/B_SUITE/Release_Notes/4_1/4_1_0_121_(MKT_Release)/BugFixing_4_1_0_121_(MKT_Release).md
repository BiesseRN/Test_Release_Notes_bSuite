**LISTA PROBLEMI RISOLTI**
***
**B_SUITE 4.1.0.121**  

**Bug 196134: [bSuite]: PR00018546: bsolid: macro con aggregato da sotto da errore di collisione in ottimizzazione**  

NOTA: è necessario cancellare e reinserire manualmente le macro

***

**Bug 196426: PR00018923: bsolid: crash istantaneo aprendo alcune macro create sulla stessa 4.0**  

***

**Bug 196427: PR00019010 [PAV] : problema con il PAV ed il passo di rotazione.**  

NOTE: Per ragioni prestazionali è fortemente consigliato al cliente di usare il parametro 0 nel passo di discretizzazione.  
In questo modo il PAV riesce a scegliere il passo corretto in funzione delle dimensioni del pannello ed ad avere una velocità di esecuzione ottimale.  

![Image Not Found](Software/B_SUITE/Release_Notes/4_1/4_1_0_121_(MKT_Release)/Image/Bug_196427_01.png) 

***

**Bug 196439: [bSuite]: PR00018817: bsolid: opzione di attrezzaggio "Lettura da CN" non riporta mai la posizione del piano 10**  

NOTA: è necessario eseguire l’import del modello macchina

***

**Bug 196535: PR00019128: bSolid: comando "Salva macro con nome" non abilitato nel modulo BASE**  

***

**Bug 196688: [bNest]: PR00019091: bNest: problema con fresature a ghigliottina.**  

***

**Bug 196820: LUA ToolLogic P122: errata visualizzazione TP15 lama in simulazione macchina**  

NOTA: è necessario eseguire l’import del modello macchina con il check “preservare i dati” abilitato.

***

**Bug 196872: [OT] LUA ToolLogic P120: problemi modello 3D**  

NOTA: è necessario eseguire l’import del modello macchina con il check “preservare i dati” abilitato e avere una versione PLC uguale o superiore alla 16.2.34.005.

***

**Bug 197147: PR00019420: Carrelli piani HTS con la pneumatica laterale dal lato sbagliato. In realtà è a destra in bSuite è a sinistra.**  

***

**Bug 197197: [OT] - [bSuite] PR00016886: Collisione tra Multifunzione 3+1 e pezzi su due file di Finger se presente una traslazione di pezzi.**  
